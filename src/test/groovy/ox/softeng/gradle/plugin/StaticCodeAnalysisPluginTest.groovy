package ox.softeng.gradle.plugin

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.JavaLibraryPlugin
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertTrue

//import org.junit.Test
/**
 * @since 15/08/2017
 */
class StaticCodeAnalysisPluginTest {

    Project project

    @BeforeEach
    void setupProject() {
        project = ProjectBuilder.builder().withName('test').build()
        project.pluginManager.apply JavaLibraryPlugin
        project.pluginManager.apply 'ox.softeng.static-code-analysis'
    }

    @Test
    void scaPluginTasksExist() {
        project.evaluate()
        ['assemble', 'build', 'check'].each {
            assertNotNull "java-library task '${it}' does not exist", project.tasks.findByName(it)
        }

        Task t = project.tasks.findByName('staticCodeAnalysis')
        assertNotNull "sca task 'staticCodeAnalysis' does not exist", t
        assertEquals 'Group should be default', 'reporting', t.group

        ['checkstyleMain', 'checkstyleTest', 'cpdCheck', 'spotbugsMain', 'spotbugsTest',
         'pmdMain', 'pmdTest'].each {
            t = project.tasks.findByName(it)
            assertNotNull "sca task '${it}' does not exists", t
            assertEquals 'Group should be default', 'static-code-analysis', t.group
        }
    }

    @Test
    void ciModeCheckDisabled() {
        project.evaluate()
        assertFalse "Not running in ci mode", project.sca.ciEnabled
        Task fbm = project.tasks.spotbugsMain
        assertFalse "Not in ci mode, xml disabled", fbm.reports.xml.enabled
        assertTrue "Not in ci mode, html enabled", fbm.reports.html.enabled
    }

    @Test
    void ciModeCheckEnabled() {
        project.sca.ciEnabled = true
        project.evaluate()
        assertTrue "Running in ci mode", project.sca.ciEnabled
        Task fbm = project.tasks.spotbugsMain
        assertTrue "In ci mode, xml enabled", fbm.reports.xml.enabled
        assertFalse "In ci mode, html disabled", fbm.reports.html.enabled
    }

    @Test
    void checkCheckstyleFileLoaded(){
        project.evaluate()
        assertNotNull "file should exist",project.checkstyle.configFile
    }
}
