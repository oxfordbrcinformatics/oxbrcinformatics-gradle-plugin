package ox.softeng.gradle.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.plugins.ide.idea.IdeaPlugin

/**
 * @since 17/08/2017
 */
class OxBrcIdeaPlugin implements Plugin<Project> {

    @Override
    void apply(Project target) {
        target.pluginManager.apply IdeaPlugin

        if (target == target.rootProject) {
            configureProjectDefaults(target)
            configureWorkspaceDefaults(target)
        }

        configureModuleDefaults(target)
    }

    void configureModuleDefaults(Project target) {
        target.idea.module {
            excludeDirs += target.file('.idea')
            excludeDirs += target.file('deploy')
            excludeDirs += target.file('.shelf')
            excludeDirs += target.file('classes')
            excludeDirs += target.file('logs')
            excludeDirs += target.file('.ideaDataSources')
            excludeDirs += target.file('dataSources')
            excludeDirs += target.file('reports')

            downloadJavadoc = true
            downloadSources = true

            // This is required to ensure JPA entity scanning works
            if (target.file('src/main/resources/META-INF/persistence.xml').exists()) outputDir = target.file('build/classes/main')

            iml.withXml {provider ->
                /**
                 * Add gradle setup to the module
                 */
                provider.node.@'external.linked.project.path' = '$MODULE_DIR$'
                provider.node.@'external.root.project.path' = '$MODULE_DIR$'
                provider.node.@'external.system.id' = 'GRADLE'
                provider.node.@'external.system.module.group' = ''
                provider.node.@'external.system.module.version' = target.version
            }
        }
    }

    void configureProjectDefaults(Project target) {
        target.idea.project {
            languageLevel = target.rootProject.sourceCompatibility
            vcs = 'Git'
            ipr.withXml {provider ->
                /**
                 * Setup the gradle settings to allow integration into intellij without intellig getting confused
                 */
                String GRADLE_HOME = System.getenv('GRADLE_HOME') ? System.getenv('GRADLE_HOME') : ''
                def moduleList = modules.collect {
                    String contRoot = it.contentRoot.toURI().toURL().toString()
                    String root = target.rootDir.toURI().toURL().toString()
                    String path = contRoot.replace(root, '$PROJECT_DIR$/')
                    path.endsWith('/') ? path.substring(0, path.lastIndexOf('/')) : path
                }

                def gradleSettings = {
                    option(name: 'linkedExternalProjectsSettings') {
                        GradleProjectSettings() {
                            option(name: 'distributionType', value: 'DEFAULT_WRAPPED')
                            option(name: 'externalProjectPath', value: '$PROJECT_DIR$')
                            option(name: 'gradleHome', value: GRADLE_HOME)
                            option(name: 'modules') {

                                set(comment: "${modules.collect {it.name}.join(',')}") {
                                    for (String m : moduleList) {
                                        option(value: m)
                                    }
                                }
                            }
                            option(name: 'useAutoImport', value: 'true')
                        }
                    }
                }

                Node gradleNode = provider.node.component.find {it.@name == 'GradleSettings'}
                if (gradleNode != null) {
                    provider.node.remove(gradleNode)
                }
                provider.node.component[0].plus {
                    Component(name: 'GradleSettings', gradleSettings)
                }

                if (provider.node.component.find {it.@name == 'GradleUISettings'} == null) {
                    provider.node.component[0].plus {
                        Component(name: 'GradleUISettings') {
                            setting(name: 'root')
                        }
                    }
                }
            }
        }
    }

    void configureWorkspaceDefaults(Project target) {
        target.idea.workspace.iws.withXml {provider ->

            def runManager = provider.node.component.find {it.@name == 'RunManager'}

            /**
             * Setup JUnit defaults to have the extra JVM options we know we need
             */
            def junitDefaults = runManager.configuration.find {it.@type == 'JUnit'}
            if (junitDefaults != null) {
                def workingDir = junitDefaults.option.find {it.@name == 'WORKING_DIRECTORY'}
                workingDir.@value = '$MODULE_DIR$'
            }
            def applicationDefaults = runManager.configuration.find {it.@type == 'Application'}
            if (applicationDefaults != null) {
                def workingDir = applicationDefaults.option.find {it.@name == 'WORKING_DIRECTORY'}
                workingDir.@value = '$MODULE_DIR$'
            }

            def propertiesComponent = provider.node.component.find {
                it.@name == 'PropertiesComponent'
            }
            def dynamicclasspath = propertiesComponent.property.find {it.@name == 'dynamic.classpath'}
            if (dynamicclasspath != null) {
                dynamicclasspath.@value == 'true'
            }
            else {
                propertiesComponent.property[-1].plus {
                    property(name: 'dynamic.classpath', value: 'true')
                }
            }
        }
    }
}
