# Oxford BRC Informatics Gradle Plugins

The following plugins are available (these are their IDs to be used when applying):

* `ox.softeng.integration-test`
* `ox.softeng.ox-brc-base`
* `ox.softeng.ox-brc-idea`
* `ox.softeng.ox-brc-standard`
* `ox.softeng.static-code-analysis`


* (`ox.softeng.ox-brc-artifactory` - Deprecated)

To use the plugins add the following to your build file

```groovy
buildscript {
    repositories {
        mavenLocal()
        mavenCentral()

    }
    dependencies {
        classpath 'ox.softeng.gradle:oxbrcinformatics-gradle-plugins:+'
    }
}

apply plugin: '<plugin-id>'
```
## ox.softeng.ox-brc-idea

Extends the `idea` plugin by setting some standard default values for the configurations

## ox.softeng.ox-brc-base

Adds a series of plain tasks along with the gradle plugins

* base
* project-reports
* ox.softeng.ox-brc-idea

## ox.softeng.ox-brc-standard

Extends the `ox.softeng.ox-brc-base` plugin adding java based tasks and the following gradle plugins

* java-library
* maven
* maven

## ox.softeng.integration-test

Extends the `ox.softeng.ox-brc-base` plugin adding java and a sourceset for integration tests along with the necessary configurations and tasks.

## ox.softeng.static-code-analysis

Adds the java-library plugin and then all the static code analysis plugins

* findbugs
* checkstyle
* jdepend
* pmd
* cpd

Sets a lot of defaults and report configurations along with a single task to run all these applied analyis tasks.

---

Also, a deprecated plugin:
### ox.softeng.ox-brc-artifactory

Extends the artifactory plugin setting defaults for use with the Oxford BRC Artifactory server

*The Oxford BRC Artifactory server has been removed and so this plugin has been deprecated.* 