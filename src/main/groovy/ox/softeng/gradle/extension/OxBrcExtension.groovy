package ox.softeng.gradle.extension

/**
 * @since 18/08/2017
 */
class OxBrcExtension {

    private Map<String, Object> store = [:]

    def propertyMissing(String name) {
        store.get(name)
    }

    def propertyMissing(String name, def arg) {
        store.put(name, arg)
    }
}
