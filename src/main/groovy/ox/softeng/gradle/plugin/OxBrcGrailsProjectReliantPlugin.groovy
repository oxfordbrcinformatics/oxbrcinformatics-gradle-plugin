package ox.softeng.gradle.plugin

import ox.softeng.gradle.Versions

import io.spring.gradle.dependencymanagement.DependencyManagementPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.type.ArtifactTypeDefinition
import org.gradle.api.plugins.GroovyPlugin
import org.gradle.api.plugins.quality.CodeNarc
import org.gradle.api.plugins.quality.CodeNarcPlugin
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.TestReport

/**
 * @since 08/11/2017
 */
class OxBrcGrailsProjectReliantPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.pluginManager.apply OxBrcStandardPlugin
        project.pluginManager.apply StaticCodeAnalysisPlugin
        project.pluginManager.apply DependencyManagementPlugin
        project.pluginManager.apply GroovyPlugin
        project.pluginManager.apply CodeNarcPlugin

        project.configurations {
            apiElements {
                outgoing.variants.getByName('classes').artifact(
                    file: project.tasks.compileGroovy.destinationDir,
                    type: ArtifactTypeDefinition.JVM_CLASS_DIRECTORY,
                    builtBy: project.tasks.compileGroovy)
            }
        }

        project.dependencyManagement {
            imports {
                mavenBom "org.grails:grails-bom:${project.ext.grailsVersion}"
                mavenBom "org.springframework.boot:spring-boot-dependencies:${project.ext.springBootVersion}"
            }
            dependencies {

                dependencySet(group: 'org.springframework.boot', version: project.ext.springBootVersion) {
                    entry "spring-boot-starter-logging"
                    entry "spring-boot-autoconfigure"
                    entry "spring-boot-starter-actuator"
                    entry "spring-boot-starter-tomcat"
                }

                dependency("org.apache.tomcat:tomcat-jdbc:${project.dependencyManagement.importedProperties['tomcat.version']}")
                dependency "org.grails.plugins:hibernate5:${(project.dependencyManagement.importedProperties['gorm.hibernate.version'] - ".RELEASE")}"
                ['core', 'simpe', 'web', 'rest-client', 'gorm', 'gorm-validation', 'gorm-support', 'hibernate-core', 'gorm-test', 'hibernate5'
                ].each {name ->
                    dependency "org.grails:grails-datastore-$name:${project.dependencyManagement.importedProperties['gorm.version']}"
                }
                ['aop', 'aspects', 'beans', 'context-support', 'context', 'core', 'expression', 'instrument', 'jdbc', 'jms', 'messaging', 'orm',
                 'oxm', 'test', 'tx', 'web', 'webmvc', 'websocket'
                ].each {name ->
                    dependency "org.springframework:spring-$name:${project.dependencyManagement.importedProperties['spring.version']}"
                }
                dependency "org.grails.plugins:views-json:${project.dependencyManagement.importedProperties['views-json.version']}"
                dependency "org.grails.plugins:views-json-templates:" +
                           "${project.dependencyManagement.importedProperties['views-json-templates.version']}"
                dependency "org.grails.plugins:views-markup:${project.dependencyManagement.importedProperties['views-markup.version']}"

            }
            applyMavenExclusions false
        }

        ClassLoader classLoader = getClass().getClassLoader()
        InputStream is = classLoader.getResourceAsStream('codenarc_rules.groovy')

        project.codenarc {
            toolVersion = Versions.CODENARC_VERSION
            config = project.resources.text.fromString(is.text)
            reportFormat = 'html'
            ignoreFailures = true
        }

        createBuildPropertiesTask(project)

        project.afterEvaluate {

            project.tasks.withType(CodeNarc) {
                group project.sca.taskGroup
                mustRunAfter project.tasks.withType(Test), project.tasks.withType(TestReport)
                exclude '**/*.sql'
            }

            project.tasks.findByName('staticCodeAnalysis')?.dependsOn project.tasks.withType(CodeNarc)

            project.tasks.withType(GroovyCompile) {task ->
                task.onlyIf {
                    !task.source.empty
                }
            }
        }
    }

    protected Task createBuildPropertiesTask(Project project) {

        File resourcesDir = project.sourceSets.main.output.resourcesDir
        File buildInfoFile = new File(resourcesDir, "META-INF/grails.build.info")


        Task buildPropertiesTask = project.tasks.create("buildProperties")
        Map<String, Object> buildPropertiesContents = ['grails.env'            : 'test',
                                                       'info.app.name'         : project.name,
                                                       'info.app.version'      :
                                                           project.version instanceof Serializable ? project.version : project.version.toString(),
                                                       'info.app.grailsVersion': project.properties.get('grailsVersion')]

        buildPropertiesTask.inputs.properties(buildPropertiesContents)
        buildPropertiesTask.outputs.file(buildInfoFile)
        buildPropertiesTask.doLast {
            project.buildDir.mkdirs()
            ant.mkdir(dir: buildInfoFile.parentFile)
            ant.propertyfile(file: buildInfoFile) {
                for (me in buildPropertiesTask.inputs.properties) {
                    entry key: me.key, value: me.value
                }
            }
        }

        project.afterEvaluate {
            TaskContainer tasks = project.tasks
            tasks.findByName("processResources")?.dependsOn(buildPropertiesTask)
        }
    }
}
