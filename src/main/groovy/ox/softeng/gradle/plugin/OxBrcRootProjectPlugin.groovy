package ox.softeng.gradle.plugin

import ox.softeng.gradle.Versions
import ox.softeng.gradle.extension.RootProjectExtension

import de.aaschmid.gradle.plugins.cpd.CpdPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.LogLevel
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.testing.TestReport
import org.gradle.internal.logging.ConsoleRenderer
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.tasks.JacocoReport

/**
 * @since 17/08/2017
 */
class OxBrcRootProjectPlugin implements Plugin<Project> {

    private static final Logger logger = Logging.getLogger(OxBrcRootProjectPlugin.class)

    public static final String EXTENSION = 'oxbrcrp'

    @Override
    void apply(Project target) {

        target.pluginManager.apply OxBrcBasePlugin
//        target.pluginManager.apply OxBrcArtifactoryPlugin
        target.pluginManager.apply CpdPlugin
        target.pluginManager.apply JacocoPlugin

        target.extensions.create(EXTENSION, RootProjectExtension)
        if (!target.rootProject.extensions.findByName(EXTENSION)) {
            target.rootProject.extensions.create(EXTENSION, RootProjectExtension)
        }

        target.reporting.baseDir = 'reports'

        target.tasks.htmlDependencyReport {
            projects = target.allprojects
        }

        target.jacoco {
            toolVersion = Versions.JACOCO_VERSION
        }

        /**
         * Unit Test task,
         * This task will generate a TestReport in HTML format for the tests which it reports on
         * Any unit tests/test tasks should be set to be reported on and depended on by this task
         */
        target.task('unitTest', type: TestReport) {
            description = 'Run Unit Tests and compile results into HTML report.'
            destinationDir = target.file({"${target.reporting.baseDir}/tests/unitTest"})
            group = 'testing'
            doLast {
                logger.info("Test failure count: {}", target."${EXTENSION}".unitTestFailures)
                if (target."${EXTENSION}".unitTestFailures) {
                    String grammer = target."${EXTENSION}".unitTestFailures == 1 ? 'was 1 unit test failure' :
                                     "were ${target."${EXTENSION}".unitTestFailures} unit test failures"
                    File indexFile = target.file({"$destinationDir/index.html"})
                    String reportUrl = new ConsoleRenderer().asClickableFileUrl(indexFile)
                    logger.quiet("There $grammer. See the report at $reportUrl. (Ctrl+click the URL to open)")
                }
            }
        }

        target.task('test') {
            dependsOn target.tasks.unitTest
            doLast {
                logger.log(LogLevel.WARN, "!! `gradle test` in the root project is a catching task. !!\n" +
                                          "It is much more appropriate to run:\n" +
                                          "* `gradle unitTest`\n" +
                                          "* `gradle integrationTest`\n"
                )
            }
        }

        target.task('integrationTest', type: TestReport) {
            description = 'Compile integraiton test results into HTML report. This will NOT execute the tests'
            destinationDir = target.file({"${target.reporting.baseDir}/tests/integrationTest"})
            group = 'testing'
            mustRunAfter target.tasks.unitTest
            doLast {
                logger.info("Test failure count: {}", target."${EXTENSION}".integrationTestFailures)
                if (target."${EXTENSION}".integrationTestFailures) {
                    String grammer = target."${EXTENSION}".integrationTestFailures == 1 ? 'was 1 integration test failure' :
                                     "were ${target."${EXTENSION}".integrationTestFailures} integration test failures"
                    File indexFile = target.file({"$destinationDir/index.html"})
                    String reportUrl = new ConsoleRenderer().asClickableFileUrl(indexFile)
                    logger.quiet("There $grammer. See the report at $reportUrl. (Ctrl+click the URL to open)")
                }
            }
        }

        target.task('jacocoRootReport', type: JacocoReport) {
            group = 'reporting'
            description = 'Generates an aggregate report from all subprojects'

            reports {
                html.required = true
                xml.required = true
            }

            sourceDirectories.from {target.subprojects.sourceSets.main.allSource.srcDirs}
            classDirectories.from {target.subprojects.sourceSets.main.output.classesDir}

            doFirst {
                executionData.setFrom executionData.findAll {it.exists()}
            }
            mustRunAfter target.tasks.unitTest, target.tasks.integrationTest
        }

        target.tasks.cpdCheck {
            mustRunAfter target.tasks.unitTest, target.tasks.integrationTest, target.tasks.jacocoRootReport
        }

        target.tasks.check {
            dependsOn target.tasks.jacocoRootReport, target.tasks.cpdCheck, target.tasks.unitTest, target.tasks.integrationTest
        }

        target.subprojects {subproject ->
            subproject.afterEvaluate {
                ['clean', 'assemble', 'build', 'unitTest', 'integrationTest', 'staticCodeAnalysis', 'check', 'compile'].each {task ->
                    target."$task".dependsOn subproject.tasks."$task"
                }

                target.tasks.jacocoRootReport.mustRunAfter subproject.tasks.jacocoTestReport
                target.tasks.install.dependsOn subproject.tasks.install

                subproject.tasks.test.afterSuite {td, tr ->
                    if (!(td.parent)) {
                        target.oxbrcrp.unitTestFailures = target.oxbrcrp.unitTestFailures + tr.failedTestCount
                    }
                }
                target.tasks.unitTest.dependsOn subproject.tasks.test
                target.tasks.unitTest.reportOn subproject.tasks.test

                subproject.tasks.integrationTest.afterSuite {td, tr ->
                    if (!(td.parent)) {
                        target.oxbrcrp.integrationTestFailures = target.oxbrcrp.integrationTestFailures + tr.failedTestCount
                    }
                }
                target.tasks.integrationTest.dependsOn subproject.tasks.integrationTest
                target.tasks.integrationTest.reportOn subproject.tasks.integrationTest

                subproject.tasks.check.dependsOn target.tasks.cpdCheck

                if (subproject.tasks.findByName('test'))
                    target.jacocoRootReport.executionData subproject.tasks.test
                if (subproject.tasks.findByName('integrationTest'))
                    target.jacocoRootReport.executionData subproject.tasks.integrationTest
            }
        }
    }
}
