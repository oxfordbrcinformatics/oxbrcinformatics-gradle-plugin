package ox.softeng.gradle.plugin

import ox.softeng.gradle.Versions

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.plugins.ApplicationPlugin
import org.gradle.api.plugins.JavaLibraryPlugin
import org.gradle.api.plugins.ProjectReportsPlugin
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.api.tasks.testing.Test
import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension

import java.util.concurrent.TimeUnit

/**
 * @since 15/08/2017
 */
class OxBrcStandardPlugin implements Plugin<Project> {

    private static final Logger logger = Logging.getLogger(OxBrcStandardPlugin.class)

    @Override
    void apply(Project project) {
        project.pluginManager.apply OxBrcIdeaPlugin
        project.pluginManager.apply ProjectReportsPlugin
        project.pluginManager.apply JavaLibraryPlugin
        project.pluginManager.apply MavenPublishPlugin
        project.pluginManager.apply JacocoPlugin

        if (project.hasProperty('mainClass')) project.pluginManager.apply ApplicationPlugin

        // This is required to ensure JPA entity scanning works
        // We also need to make sure under ALL circumstances the resources are processed when java is compiled
        if (project.file('src/main/resources/META-INF/persistence.xml').exists()) {
            project.sourceSets.main.output.resourcesDir = project.sourceSets.main.output.classesDir
            project.compileJava.finalizedBy project.processResources
        }

        OxBrcBasePlugin.configureProject(project)

        project.tasks.compile.dependsOn project.tasks.classes, project.tasks.testClasses
        project.assemble.dependsOn project.tasks.compile

        project.task('cleanTestResults', type: Delete) {
            group = 'clean'
            delete 'build/test-results'
        }

        project.task('jenkinsClean') {
            group = 'clean'
            dependsOn project.tasks.cleanLogs, project.tasks.cleanTestResults, project.tasks.cleanReports
        }

        project.clean {
            dependsOn project.tasks.jenkinsClean
        }

        project.test {
            group 'testing'
            useJUnitPlatform()
            ignoreFailures = true
        }

        project.javadoc.failOnError false
        project.java {
            if (project.gradle.startParameter.getTaskNames().size() == 1 && project.gradle.startParameter.getTaskNames().first() in ['publishToMavenLocal', 'pTML']) {
                logger.info('Not producing javadoc')
            } else {
                withJavadocJar()
            }
            withSourcesJar()
            toolchain {
                languageVersion = JavaLanguageVersion.of(17)
            }
        }

        boolean publishMavenJar = true
        String disableMavenJarPublication = project.findProperty('disableMavenJarPublication')
        if (disableMavenJarPublication) {
            publishMavenJar = !disableMavenJarPublication.toBoolean()
        }

        if (publishMavenJar) {
            project.publishing {
                publications {
                    mavenJar(MavenPublication) {
                        from project.components.java
                        //                    artifact sourcesJar
                        //                    artifact javadocJar
                    }
                }
            }
        }

        project.tasks.htmlDependencyReport.group = 'reporting'
        project.tasks.findAll {it.group == 'IDE'}.each {it.group = null}


        project.jacoco {
            toolVersion = Versions.JACOCO_VERSION
        }

        // Assumes tests have been run, otherwise gradle will skip
        project.jacocoTestReport {
            group 'reporting'
            reports {
                xml.required = false
                html.required = true
            }
            dependsOn 'updateJacocoExecutionData'
            executionData.setFrom([])
        }

        project.tasks.create('updateJacocoExecutionData') {
            doLast {
                project.tasks.withType(Test).each {tk ->
                    final JacocoTaskExtension extension = tk.getExtensions().findByType(JacocoTaskExtension)
                    if (extension && extension.getDestinationFile().exists()) {
                        project.jacocoTestReport.executionData(extension.getDestinationFile())
                        project.jacocoTestReport.mustRunAfter(tk)
                    }
                }
            }
            mustRunAfter(project.tasks.withType(Test))
        }

        project.check.dependsOn project.tasks.jacocoTestReport

        if (project.hasProperty('mainClass')) {

            project.mainClassName = project.mainClass

            if (project.hasProperty('applicationScriptName')) {
                project.applicationName = project.applicationScriptName
            }

            // If supplied any default args then pass them into run scripts
            if (project.hasProperty('defaultApplicationJvmArgs')) {
                project.applicationDefaultJvmArgs += ((String) project.defaultApplicationJvmArgs).split(',')
            }
        }
        project.afterEvaluate {

            if (project.hasProperty('mainClass')) {
                project.applicationDefaultJvmArgs = ["-DapplicationVersion=${project.version}"]
            }

            project.tasks.withType(JavaCompile) {JavaCompile compile ->
                compile.options.fork = true
                compile.options.encoding = 'UTF-8'
                //compile.options.incremental = useIncrementalJavaBuilding.toBoolean()
                compile.options.compilerArgs.add('-Xlint:unchecked')
                compile.options.compilerArgs.add('-Xlint:deprecation')
            }
            project.tasks.withType(GroovyCompile) {GroovyCompile compile ->
                compile.options.fork = true
                compile.options.encoding = 'UTF-8'
            }
            project.tasks.withType(Jar) {
                Map attrs = [
                    "Created-By"            : "${JavaVersion.current().toString()} JVM, ${project.gradle.gradleVersion} Gradle",
                    "Specification-Title"   : "$project.rootProject.name $project.name Classes",
                    "Specification-Version" : project.version,
                    "Implementation-Title"  : "${project.group.toLowerCase()}.${project.name.toLowerCase()}",
                    "Implementation-Version": "${project.version}.dev",
                    "Implementation-Vendor" : "Oxford University",
                ]
                if (project.hasProperty('mainClass'))
                    attrs.'Main-Class' = project.mainClass

                manifest {
                    attributes(attrs)
                }
                // sourceCompatibility = project.sourceCompatibility
            }
            project.tasks.withType(Test) {
                reports.html.destination = project.file("${project.reporting.baseDir}/tests/${name}")
                maxParallelForks = Runtime.runtime.availableProcessors().intdiv(2) ?: 1
            }

            project.configurations.all {
                // check for updates every build
                resolutionStrategy.cacheChangingModulesFor(0, TimeUnit.SECONDS)
            }

//            logger.quiet "Project: ${project.name} > group: ${project.group}, version: ${project.version}"
        }
    }
}
