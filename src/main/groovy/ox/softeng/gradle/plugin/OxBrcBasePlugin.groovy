package ox.softeng.gradle.plugin

import ox.softeng.gradle.extension.OxBrcExtension

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.plugins.ProjectReportsPlugin
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.wrapper.Wrapper

/**
 * @since 17/08/2017
 */
class OxBrcBasePlugin implements Plugin<Project> {

    private static final Logger logger = Logging.getLogger(OxBrcBasePlugin.class);

    @Override
    void apply(Project project) {
        project.pluginManager.apply BasePlugin
        project.pluginManager.apply OxBrcIdeaPlugin
        project.pluginManager.apply ProjectReportsPlugin

        configureProject(project)

//        project.afterEvaluate {
//            logger.quiet "Project: ${project.name} > group: ${project.group}, version: ${project.version}"
//        }
    }

    static void configureProject(Project project) {

        if (!project.extensions.findByName('oxbrc'))
            project.extensions.create('oxbrc', OxBrcExtension)

        project.repositories {
            mavenLocal()
            mavenCentral()
        }

        project.htmlDependencyReport {
            group 'reporting'
            projects = project.allprojects
        }

        project.clean {
            group 'clean'
            delete 'out'
        }

        if (!project.tasks.findByName('cleanReports'))
            project.task('cleanReports', type: Delete) {
                group 'reporting'
                delete 'build/reports'
            }

        if (!project.tasks.findByName('show'))
            project.task('show') {
                group 'help'
                doLast {
                    logger.quiet "All seems to be working"
                }
            }

        if (!project.tasks.findByName('cleanLogs'))
            project.task('cleanLogs', type: Delete) {
                group 'clean'
                delete 'logs', 'build/logs'
            }


        if (!project.tasks.findByName('compile'))
            project.task('compile') {
                group 'build'
                description = 'Catch task for compile'
            }
        project.tasks.assemble.dependsOn project.tasks.compile

        if (project.tasks.findByName('wrapper')) {
            project.wrapper {
                gradleVersion = project.gradleVersion
                distributionType = Wrapper.DistributionType.ALL
            }
        }
    }
}
