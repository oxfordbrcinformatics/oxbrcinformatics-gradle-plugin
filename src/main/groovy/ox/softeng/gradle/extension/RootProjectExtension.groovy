package ox.softeng.gradle.extension

/**
 * @since 17/08/2017
 */
class RootProjectExtension {

    int unitTestFailures = 0
    int integrationTestFailures = 0

    int getUnitTestFailures() {
        return unitTestFailures
    }

    void setUnitTestFailures(int unitTestFailures) {
        this.unitTestFailures = unitTestFailures
    }

    int getIntegrationTestFailures() {
        return integrationTestFailures
    }

    void setIntegrationTestFailures(int integrationTestFailures) {
        this.integrationTestFailures = integrationTestFailures
    }
}
