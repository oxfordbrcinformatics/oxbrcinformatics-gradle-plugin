package ox.softeng.gradle.plugin

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.JavaCompile

/**
 * @since 17/08/2017
 */
@Deprecated
class OxBrcArtifactoryPlugin implements Plugin<Project> {

    private static final Logger logger = Logging.getLogger(OxBrcArtifactoryPlugin)

    @Override
    void apply(Project project) {

        project.pluginManager.apply MavenPublishPlugin

        project.afterEvaluate {
            project.tasks.withType(Jar) {
                manifest {
                    attributes([
                        "Created-By"            : "${JavaVersion.current().toString()} JVM, ${project.gradle.gradleVersion} Gradle",
                        "Specification-Title"   : "$project.rootProject.name $project.name Classes",
                        "Specification-Version" : project.version,
                        "Implementation-Title"  : "${project.group.toLowerCase()}.${project.name.toLowerCase()}",
                        "Implementation-Version": "${project.version}.dev",
                        "Implementation-Vendor" : "Oxford University",
                    ])
                }
                group 'archives'
            }
            project.tasks.withType(JavaCompile) {JavaCompile compile ->
                compile.options.encoding = 'UTF-8'
                compile.options.compilerArgs.add('-Xlint:unchecked')
                compile.options.compilerArgs.add('-Xlint:deprecation')
            }
        }

        if (project.hasProperty('artifactory_user') && project.hasProperty('artifactory_apiKey')) {
            project.publishing {
                repositories {
                    maven {
                        name 'artifactory'
                        credentials {
                            username "${project.artifactory_user}"
                            password "${project.artifactory_apiKey}"
                        }
                        url project.version.endsWith('SNAPSHOT') ? "https://jenkins.cs.ox.ac.uk/artifactory/libs-snapshot-local" :
                            "https://jenkins.cs.ox.ac.uk/artifactory/libs-release-local"
                    }
                }

            }
        } else if (project.rootProject == project) {
            logger.quiet "Artifactory publishing not available as properties 'artifactory_user' & 'artifactory_apiKey' need to be defined in " +
                         "~/.gradle/gradle.properties"
        }

    }
}
