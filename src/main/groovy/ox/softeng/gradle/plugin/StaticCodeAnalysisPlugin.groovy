package ox.softeng.gradle.plugin

import ox.softeng.gradle.Versions
import ox.softeng.gradle.extension.StaticCodeAnalysisExtension

import com.github.spotbugs.snom.SpotBugsPlugin
import com.github.spotbugs.snom.SpotBugsTask
import de.aaschmid.gradle.plugins.cpd.CpdPlugin
import groovy.util.logging.Slf4j
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.plugins.InvalidPluginException
import org.gradle.api.plugins.quality.Checkstyle
import org.gradle.api.plugins.quality.CheckstylePlugin
import org.gradle.api.plugins.quality.CodeNarc
import org.gradle.api.plugins.quality.CodeNarcPlugin
import org.gradle.api.plugins.quality.Pmd
import org.gradle.api.plugins.quality.PmdPlugin
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.TestReport
import org.sonarqube.gradle.SonarQubeExtension
import org.sonarqube.gradle.SonarQubePlugin
import org.sonarqube.gradle.SonarQubeTask
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension


/**
 * @since 15/08/2017
 */
@Slf4j
@SuppressWarnings("UnstableApiUsage")
class StaticCodeAnalysisPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.pluginManager.apply BasePlugin
        project.pluginManager.apply CheckstylePlugin
        project.pluginManager.apply SpotBugsPlugin
        project.pluginManager.apply PmdPlugin
        project.pluginManager.apply CodeNarcPlugin
        project.pluginManager.apply SonarQubePlugin
        project.pluginManager.apply CpdPlugin

        project.extensions.create 'sca', StaticCodeAnalysisExtension

        project.task('staticCodeAnalysis', group: 'reporting') {
            description = 'Catch task for all SCA tasks'
            mustRunAfter project.tasks.withType(Test), project.tasks.withType(TestReport)
            doLast {
                logger.quiet "\n==> Ran Static Code Analysis in ${project.sca.ciEnabled ? 'CI' : 'local'} mode <=="
            }
            dependsOn project.tasks.withType(SpotBugsTask),
                      project.tasks.withType(Checkstyle),
                      project.tasks.withType(Pmd),
                      project.tasks.withType(CodeNarc)
        }

        project.task('collateTestResultsForSonar', type: Copy) {
            enabled = project.testResultsDir.exists()
            from(project.testResultsDir) {
                include '**/TEST-*.xml'
            }
            into project.file("${project.buildDir}/sonar-test-results")
            duplicatesStrategy = DuplicatesStrategy.WARN
            // Flatten the hierarchy by setting the path
            // of all files to their respective basename
            eachFile {
                path = name
            }

            // Flattening the hierarchy leaves empty directories,
            // do not copy those
            includeEmptyDirs = false
            mustRunAfter project.tasks.withType(Test)
        }

        project.task('cleanSonarTestResults', type: Delete, group: 'clean') {
            group = 'clean'
            delete 'build/sonar-test-results'
        }

        configureAfterEvaluate(project)

        ClassLoader classLoader = getClass().getClassLoader()
        InputStream is = classLoader.getResourceAsStream("checkstyle.xml")

        project.checkstyle {
            // http://checkstyle.sourceforge.net/index.html
            config = project.resources.text.fromString(is.text)
            ignoreFailures = true
            toolVersion = Versions.CHECKSTYLE_VERSION
        }

        project.spotbugs {
            // https://plugins.gradle.org/plugin/com.github.spotbugs
            // https://spotbugs.github.io
            toolVersion = Versions.SPOTBUGS_VERSION
            ignoreFailures = true
            showStackTraces = false
            maxHeapSize = '2g'
        }

        project.pmd {
            // https://pmd.github.io/pmd-5.8.1/
            // https://pmd.github.io/pmd-5.8.1/pmd-java/rules/index.html

            /**
             * Running every ruleset will result in a huge number of rule violations, most of which will be unimportant.
             * Having to sort through a thousand line report to find the few you’re really interested in takes all the fun out of things.
             *
             * To find the rules, https://github.com/pmd/pmd/tree/master/pmd-java/src/main/resources/rulesets/java
             * then append the names minus the xml to `java-`
             */
            toolVersion = Versions.PMD_VERSION
            ignoreFailures = true
            ruleSets = ['category/java/errorprone.xml', 'category/java/bestpractices.xml']
        }

        project.codenarc {
            toolVersion = Versions.CODENARC_VERSION
            reportFormat = project.sca.ciEnabled ? 'xml' : 'html'
            ignoreFailures = true
        }

        project.sonarqube {
            properties {
                property 'sonar.java.pmd.reportPaths', []
                property 'sonar.java.spotbugs.reportPaths', []
                property 'sonar.java.checkstyle.reportPaths', []
                property 'sonar.groovy.codenarc.reportPaths', []
                property 'sonar.coverage.jacoco.xmlReportPaths', []
                property 'sonar.junit.reportPaths', 'build/sonar-test-results'
                property 'sonar.junit.reportsPath', 'build/sonar-test-results'
                property 'sonar.surefire.reportsPath', 'build/sonar-test-results'
            }
        }

        project.tasks.check.dependsOn project.tasks.staticCodeAnalysis
    }

    void configureAfterEvaluate(Project project) {

        project.afterEvaluate {

            StaticCodeAnalysisExtension sca = project.extensions.findByName('sca')

            // Enable user friendly reports when running locally
            project.tasks.withType(SpotBugsTask) {
                group sca.taskGroup
                reports {
                    xml {
                        required = sca.ciEnabled
                        enabled = sca.ciEnabled
                        if (sca.sonarReportSpotbugs) {
                            project.sonarqube {
                                properties {
                                    properties['sonar.java.spotbugs.reportPaths'] += getDestination()
                                }
                            }
                        }
                    }
                    html {
                        required = !sca.ciEnabled
                        enabled = !sca.ciEnabled
                    }
                }
                mustRunAfter project.tasks.withType(Test), project.tasks.withType(TestReport)
                classes = classes.filter {
                    it.name.endsWith('.class')
                }
            }

            project.tasks.withType(Checkstyle) {
                group sca.taskGroup
                reports {
                    xml {
                        required = sca.ciEnabled
                        if (sca.sonarReportCheckstyle) {
                            project.sonarqube {
                                properties {
                                    properties['sonar.java.checkstyle.reportPaths'] += getDestination()
                                }
                            }
                        }
                    }
                    html.required = !sca.ciEnabled
                }
                mustRunAfter project.tasks.withType(Test), project.tasks.withType(TestReport)
            }

            project.tasks.withType(Pmd) {
                group sca.taskGroup
                reports {
                    xml {
                        required = sca.ciEnabled
                        if (sca.sonarReportPmd) {
                            project.sonarqube {
                                properties {
                                    properties['sonar.java.pmd.reportPaths'] += getDestination()
                                }
                            }
                        }
                    }
                    html.required = !sca.ciEnabled
                }
                mustRunAfter project.tasks.withType(Test), project.tasks.withType(TestReport)
            }

            project.tasks.withType(CodeNarc) {
                group sca.taskGroup
                reports {
                    xml {
                        required = sca.ciEnabled
                        if (sca.sonarReportCodenarc) {
                            project.sonarqube {
                                properties {
                                    properties['sonar.groovy.codenarc.reportPaths'] += getDestination()
                                }
                            }
                        }
                    }
                    html.required = !sca.ciEnabled
                }
                mustRunAfter project.tasks.withType(Test), project.tasks.withType(TestReport)
            }

            project.tasks.cpdCheck {
                group 'static-code-analysis'
                language 'groovy'
            }


            project.tasks.withType(JacocoTaskExtension) {
                group 'reporting'
                reports {
                    xml {
                        required = sca.ciEnabled
                        project.sonarqube {
                            properties {
                                properties['sonar.coverage.jacoco.xmlReportPaths'] += getDestination()
                            }
                        }
                    }
                    html.required = !sca.ciEnabled
                }
                dependsOn 'updateJacocoExecutionData'
                executionData.setFrom([])
            }

            project.sonarqube {
                properties {
                    if (project.tasks.findByName('integrationTest')) {
                        def allTestDirs = project.sourceSets.test.allJava.srcDirs + project.sourceSets.integrationTest.allJava.srcDirs
                        def allTestBinaries = project.sourceSets.test.output.classesDirs + project.sourceSets.integrationTest.output.classesDirs
                        def allTestLibraries = project.sourceSets.test.compileClasspath + project.sourceSets.integrationTest.compileClasspath
                        property 'sonar.tests', "${findAllExisting(project, allTestDirs)}"
                        property 'sonar.java.test.binaries', "${findAllExisting(project, allTestBinaries)}"
                        property 'sonar.java.test.libraries', "${findAllExisting(project, allTestLibraries)}"
                        property 'sonar.groovy.jacoco.itReportPath', 'build/jacoco/integrationTest.exec,build/jacoco/functionalTest.exec'
                    }
                }
            }

            SonarQubeTask sonarQubeTask = project.tasks.findByName(SonarQubeExtension.SONARQUBE_TASK_NAME)
            if (sonarQubeTask) {
                sonarQubeTask.dependsOn 'collateTestResultsForSonar'
            } else {
                sonarQubeTask = project.rootProject.tasks.findByName(SonarQubeExtension.SONARQUBE_TASK_NAME)
                if (!sonarQubeTask) throw new InvalidPluginException('No Sonarqube task exists despite the plugin being added')
                sonarQubeTask.dependsOn project.rootProject.subprojects.collect {it.tasks.findByName('collateTestResultsForSonar')}.findAll()
            }


        }
    }

    String findAllExisting(Project project, Collection<File> files) {
        files.findAll {it.exists()}.collect {project.projectDir.relativePath(it)}.join(',')
    }

    String findAllExisting(Project project, FileCollection files) {
        findAllExisting(project, files.getFiles())
    }
}
