package ox.softeng.gradle

/**
 * @since 2019-04-01
 */
class Versions {
    public static final String JACOCO_VERSION = '0.8.8'
    public static final String CODENARC_VERSION = '3.0.0'
    public static final String CHECKSTYLE_VERSION = '10.2'
    public static final String SPOTBUGS_VERSION = '4.6.0'
    public static final String PMD_VERSION = '6.44.0'

}
