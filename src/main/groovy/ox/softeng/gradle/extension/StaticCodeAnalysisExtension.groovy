package ox.softeng.gradle.extension

/**
 * @since 15/08/2017
 */
class StaticCodeAnalysisExtension {
    boolean ciEnabled = false
    String taskGroup = 'static-code-analysis'

    boolean sonarReportCodenarc = false
    boolean sonarReportCheckstyle = true
    boolean sonarReportSpotbugs = true
    boolean sonarReportPmd = true

    boolean getCiEnabled() {
        return ciEnabled
    }

    void setCiEnabled(boolean ciEnabled) {
        this.ciEnabled = ciEnabled
    }

    void setCiEnabled(String ciEnabled) {
        this.ciEnabled = ciEnabled.toBoolean()
    }

    boolean getSonarReportCodenarc() {
        return sonarReportCodenarc
    }

    void setSonarReportCodenarc(boolean sonarReportCodenarc) {
        this.sonarReportCodenarc = sonarReportCodenarc
    }

    boolean getSonarReportCheckstyle() {
        return sonarReportCheckstyle
    }

    void setSonarReportCheckstyle(boolean sonarReportCheckstyle) {
        this.sonarReportCheckstyle = sonarReportCheckstyle
    }

    boolean getSonarReportSpotbugs() {
        return sonarReportSpotbugs
    }

    void setSonarReportSpotbugs(boolean sonarReportSpotbugs) {
        this.sonarReportSpotbugs = sonarReportSpotbugs
    }

    boolean getSonarReportPmd() {
        return sonarReportPmd
    }

    void setSonarReportPmd(boolean sonarReportPmd) {
        this.sonarReportPmd = sonarReportPmd
    }
}
