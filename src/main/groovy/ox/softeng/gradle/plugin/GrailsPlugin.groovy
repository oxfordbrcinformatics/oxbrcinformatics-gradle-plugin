package ox.softeng.gradle.plugin

import ox.softeng.gradle.Versions

import grails.views.gradle.AbstractGroovyTemplateCompileTask
import org.gradle.api.GradleException
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.logging.LogLevel
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.plugins.InvalidPluginException
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.api.tasks.javadoc.Groovydoc
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.api.tasks.testing.Test
import org.gradle.internal.os.OperatingSystem
import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.testing.jacoco.plugins.JacocoPlugin

/**
 * @since 03/03/2021
 */
class GrailsPlugin implements Plugin<Project> {

    private static final Logger logger = Logging.getLogger(GrailsPlugin)

    @Override
    void apply(Project project) {

        if (!project.pluginManager.hasPlugin('org.grails.grails-plugin') && !project.pluginManager.hasPlugin('org.grails.grails-web')) {
            throw new InvalidPluginException('Grails plugin org.grails.grails-plugin or org.grails.grails-web is required')
        }

        project.pluginManager.apply JacocoPlugin
        project.pluginManager.apply OxBrcBasePlugin
        project.pluginManager.apply OxBrcArtifactoryPlugin
        project.pluginManager.apply StaticCodeAnalysisPlugin

        // Use the pathing jar if running in windows. This is needed otherwise the classpath is too long for the idiotic windows OS
        project.grails {
            if (OperatingSystem.current().isWindows()) {
                logger.quiet('Using pathing Jar as running in Windows OS')
                pathingJar = true
            }
        }

        project.jacoco {
            toolVersion = Versions.JACOCO_VERSION
        }

        // Ensure the compile task in gradle runs all the classes tasks.
        project.compile.dependsOn(project.tasks.findAll {it.name.toLowerCase().endsWith('classes')})

        /*
         Create a way of only running integration tests and only running functional tests
         Whilst these should be inside separate source directories and a new task created, this approach is not the grails way, but we do not want
         to execute any functional tests until the integration ones have passed.
        */
        boolean integrationTests = true
        boolean functionalTests = true
        boolean parallelTesting = false
        boolean nonParallelTesting = false

        if (System.getProperty('gradle.integrationTest')) {
            integrationTests = System.getProperty('gradle.integrationTest').toBoolean()
            functionalTests = !integrationTests
        }
        if (System.getProperty('gradle.functionalTest')) {
            functionalTests = System.getProperty('gradle.functionalTest').toBoolean()
            integrationTests = !functionalTests
        }
        if (System.getProperty('gradle.parallel')) {
            parallelTesting = System.getProperty('gradle.parallel').toBoolean()
            nonParallelTesting = !parallelTesting
        }
        if (System.getProperty('gradle.nonParallel')) {
            nonParallelTesting = System.getProperty('gradle.nonParallel').toBoolean()
            parallelTesting = !nonParallelTesting
        }

        project.ext.integrationTestingOnly = integrationTests && !functionalTests
        project.ext.functionalTestingOnly = functionalTests && !integrationTests
        project.ext.parallelTestingOnly = parallelTesting && !nonParallelTesting
        project.ext.nonParallelTestingOnly = nonParallelTesting && !parallelTesting

        if (project.tasks.findByName('integrationTest')) {

            project.configurations {
                integrationTestImplementation {
                    extendsFrom testImplementation
                }
            }

            project.integrationTest {
                systemProperty 'hibernate.search.backend.directory.root', "${project.buildDir.path}/hibernate_search/${UUID.randomUUID().toString()}"
                System.properties.each {prop ->
                    systemProperty prop.key, prop.value
                }
                systemProperty 'user.dir', workingDir
                systemProperty 'grails.run.active', 'true'
                systemProperty 'grails.env', 'test'
                enabled project.file("src/integration-test").isDirectory()
                useJUnitPlatform()
            }

            project.ext.itTestsAvailable = !project.fileTree('src/integration-test/groovy').filter {File f ->
                !f.name.find(/FunctionalSpec/)
            }.isEmpty()
            project.ext.ftTestsAvailable = project.fileTree('src/integration-test/groovy').any {f ->
                f.name.find(/FunctionalSpec/)
            }

            if (project.ext.integrationTestingOnly) {
                if (project.ext.itTestsAvailable) logger.log(LogLevel.WARN, '<<>> Running integration tests only <<>>')
                project.integrationTest {
                    onlyIf {
                        project.ext.itTestsAvailable
                    }
                    filter {
                        excludeTestsMatching '*FunctionalSpec'
                    }
                }
            } else if (project.ext.functionalTestingOnly) {
                if (project.ext.ftTestsAvailable) logger.log(LogLevel.WARN, '<<>> Running functional tests only <<>>')
                project.integrationTest {
                    onlyIf {
                        project.ext.ftTestsAvailable
                    }
                    binaryResultsDirectory.set(project.file("${project.testResultsDir}/functionalTest/binary"))
                    filter {
                        includeTestsMatching '*FunctionalSpec'
                    }
                    reports {
                        junitXml.getOutputLocation().set project.file("${project.testResultsDir}/functionalTest")
                    }
                    jacoco {
                        destinationFile = project.file("${project.buildDir}/jacoco/functionalTest.exec")
                    }
                }
                project.mergeTestReports.reportOn(project.file("${project.testResultsDir}/functionalTest/binary"))
            }
        }

        project.task('cleanTestResults', type: Delete) {
            group = 'clean'
            delete 'build/test-results'
        }


        project.task('jenkinsClean') {
            group = 'clean'
            dependsOn project.tasks.cleanLogs
            dependsOn project.tasks.cleanTestResults
            dependsOn project.tasks.cleanReports
            dependsOn project.tasks.cleanSonarTestResults
        }

        project.test {
            useJUnitPlatform()
            systemProperty 'grails.root.base.dir', project.rootDir.absolutePath
            systemProperty 'grails.env', 'test'
            systemProperty 'grails.run.active', 'true'
            maxParallelForks = Runtime.runtime.availableProcessors().intdiv(2) ?: 1
        }

        if (project.tasks.findByName('mergeTestReports')) {
            logger.info('Merging test reports')
            project.test {
                finalizedBy project.tasks.getByName('mergeTestReports')
                // Turn off the HTML report as this will be generated now in the correct place by the mergeTestReports task
                reports {
                    html {
                        required = false
                    }
                }
            }
        } else {
            logger.info("Fixing test report path to ${project.file("${project.buildDir}/reports/tests/index.html")}")
            project.test {
                reports {
                    html {
                        destination = project.file("${project.buildDir}/reports/tests")
                    }
                }
            }
        }

        project.tasks.create('updateJacocoExecutionData') {
            doLast {
                if (project.file("${project.buildDir}/jacoco/functionalTest.exec").exists()) {
                    project.jacocoTestReport.executionData(project.file("${project.buildDir}/jacoco/functionalTest.exec"))
                }
                if (project.file("${project.buildDir}/jacoco/integrationTest.exec").exists()) {
                    project.jacocoTestReport.executionData(project.file("${project.buildDir}/jacoco/integrationTest.exec"))
                }
                if (project.file("${project.buildDir}/jacoco/test.exec").exists()) {
                    project.jacocoTestReport.executionData(project.file("${project.buildDir}/jacoco/test.exec"))
                }
            }
            mustRunAfter(project.tasks.withType(Test))
        }

        if (project.file('grails-app/conf/db/migration').exists()) {
            project.task('verifyFlywayMigrationVersions', group: 'verification') {
                doLast {
                    logger.info("${project.name} has flyway migration")
                    FileTree migrationTree = project.fileTree('grails-app/conf/db/migration') {
                        include '**/*.sql'
                    }
                    List<String> names = migrationTree.collect {it.name}
                    logger.info("Found migrations\n  ${names.join('\n  ')}")

                    List<String> versions = names.collect {it.find(/(V\d+_\d+_\d+)/)}.findAll()
                    logger.info("Found versions\n  ${versions.join('\n  ')}")

                    if (versions.size() != versions.toSet().size()) {
                        List<String> duplicatedVersions = versions
                            .groupBy {it}
                            .findAll {k, v -> v.size() > 1}
                            .collect {k, v -> k}
                        throw new GradleException("Flyway migration versions are not unique in project ${project.name}\n" +
                                                  "The following versions are duplicated:\n  >> ${duplicatedVersions.join('\n  >> ')}")
                    }
                }
            }
        }

        project.javadoc {
            failOnError false
            if (JavaVersion.current().isJava9Compatible()) {
                options.addBooleanOption('html5', true)
            }
        }

        project.java {
            if (project.gradle.startParameter.getTaskNames().size() == 1 && project.gradle.startParameter.getTaskNames().first() in ['publishToMavenLocal', 'pTML']) {
                logger.info('Not producing javadoc')
            } else {
                withJavadocJar()
            }
            withSourcesJar()
            toolchain {
                languageVersion = JavaLanguageVersion.of(17)
            }
        }

        project.publishing {
            publications {
                mavenJar(MavenPublication) {
                    from project.components.java
                }
            }
        }

        project.afterEvaluate {

            project.tasks.withType(JavaCompile) {JavaCompile c ->
                c.options.fork = true
                c.options.encoding = 'UTF-8'
                //compile.options.incremental = useIncrementalJavaBuilding.toBoolean()
                c.options.compilerArgs.add('-Xlint:unchecked')
                c.options.compilerArgs.add('-Xlint:deprecation')
            }
            project.tasks.withType(GroovyCompile) {GroovyCompile c ->
                c.options.fork = true
                c.options.encoding = 'UTF-8'
            }
            project.tasks.withType(Jar) {
                Map attrs = [
                    "Created-By"            : "${JavaVersion.current().toString()} JVM, ${project.gradle.gradleVersion} Gradle",
                    "Specification-Title"   : "$project.rootProject.name $project.name Classes",
                    "Specification-Version" : project.version,
                    "Implementation-Title"  : "${project.group.toLowerCase()}.${project.name.toLowerCase()}",
                    "Implementation-Version": "${project.version}.dev",
                    "Implementation-Vendor" : "Oxford University",
                ]
                if (project.hasProperty('mainClass'))
                    attrs.'Main-Class' = project.mainClass

                manifest {
                    attributes(attrs)
                }
                group 'archives'
            }

            def addtlJvmArgs = ['-XX:+UseG1GC', '-XX:+UseStringDeduplication', '-XX:+UseCompressedOops', '-Dfile.encoding=UTF-8',
                                '-XX:ReservedCodeCacheSize=128M', '-XX:+UseCodeCacheFlushing']
            // Make sure bootRun runs with the required arguments
            project.tasks.withType(JavaExec).configureEach {
                jvmArgs(addtlJvmArgs)
                minHeapSize = '1g'
                maxHeapSize = '8g'
            }

            project.tasks.withType(Test).configureEach {testTask ->
                jvmArgs(addtlJvmArgs)
                minHeapSize = '1g'
                maxHeapSize = '8g'
            }

            // Make sure explicit dependencies on tasks exist
            def templateCompileTasks = project.tasks.withType(AbstractGroovyTemplateCompileTask)
            project.tasks.withType(Javadoc).configureEach {
                it.dependsOn templateCompileTasks
            }
            project.tasks.withType(Groovydoc).configureEach {
                it.dependsOn templateCompileTasks
            }
            if (project.tasks.findByName('compileIntegrationTestGroovy')) project.tasks.findByName('compileIntegrationTestGroovy').dependsOn templateCompileTasks
            if (project.tasks.findByName('compileTestGroovy')) project.tasks.findByName('compileTestGroovy').dependsOn templateCompileTasks

        }
    }
}
