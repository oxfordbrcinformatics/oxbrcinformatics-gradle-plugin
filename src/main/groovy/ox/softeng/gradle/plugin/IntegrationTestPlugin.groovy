package ox.softeng.gradle.plugin

import ox.softeng.gradle.Versions

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.GroovyPlugin
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.testing.Test
import org.gradle.testing.jacoco.plugins.JacocoPlugin

/**
 * @since 17/08/2017
 */
class IntegrationTestPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {

        project.pluginManager.apply JavaPlugin
        project.pluginManager.apply JacocoPlugin

        OxBrcBasePlugin.configureProject(project)

        // Add integration test configuration
        project.sourceSets {
            integrationTest {
                java.srcDirs = [project.file('src/integration-test/java')]
                resources.srcDirs = [project.file('src/integration-test/resources')]
                compileClasspath += test.runtimeClasspath
                runtimeClasspath += test.runtimeClasspath

                if (project.plugins.findPlugin(GroovyPlugin))
                    groovy.srcDirs = [project.file('src/integration-test/groovy')]
            }
        }

        project.configurations {
            integrationTestImplementation.extendsFrom testImplementation
            integrationTestRuntimeOnly.extendsFrom testRuntimeOnly
        }

        project.tasks.compile.dependsOn project.tasks.integrationTestClasses

        project.test {
            group 'testing'
            useJUnit()
            ignoreFailures = true
        }

        project.task('unitTest') {
            group = 'testing'
            description = 'Catch task for test, which are unit tests'
            dependsOn project.tasks.test
        }

        project.task('integrationTest', type: Test) {
            group = 'testing'
            useJUnit()
            ignoreFailures = true
            testClassesDirs = project.sourceSets.integrationTest.output.classesDirs
            classpath = project.sourceSets.integrationTest.runtimeClasspath
            mustRunAfter project.tasks.unitTest
        }

        project.check {
            dependsOn project.tasks.integrationTest, project.tasks.unitTest
        }

        project.jacoco {
            toolVersion = Versions.JACOCO_VERSION
        }

        project.jacocoTestReport {
            group 'reporting'
            reports {
                xml.required = false
                html.required = true
            }
        }
    }
}
